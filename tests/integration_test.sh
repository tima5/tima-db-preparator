#!/usr/bin/env bash

echo "Let's check if we can run the whole process"
echo " Get lotus"
make get-lotus
echo " Prepare lotus"
make prepare-lotus
echo " Prepare dnp"
make prepare-dnp
echo " Prepare library"
make prepare-library
echo " Form adducts"
make form-adducts

echo "We need to do quality controls, please help!"
