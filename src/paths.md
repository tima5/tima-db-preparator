# paths

path_r_python = function(x, y = NULL) { 
  if (!is.null(y)) { 
    if (language == "r") { 
      path = file.path(x, y)
    }
    if (language == "python") {
      path = os.pathjoin(x, y)
    }
  return(path)
  }
  if (is.null(y)) { 
    if (language == "r") { 
      path = file.path(x)
    }
    if (language == "python") {
      path = os.pathjoin(x)
    }
    return(path)
  }
}

## config

config = path_r_python("../config")

### default
config_default = path_r_python(config,"default")

#### dnp
config_default_dnp = path_r_python(config_default,"dnp_default.yaml")

#### library
config_default_library = path_r_python(config_default,"library_default.yaml")

#### adducts
config_default_adducts = path_r_python(config_default,"adducts_default.yaml")

### params
config_params = path_r_python(config,"params")

#### dnp
config_params_dnp = path_r_python(config_params,"dnp_params.yaml")

#### library
config_params_library = path_r_python(config_params,"library_params.yaml")

#### adducts
config_params_adducts = path_r_python(config_params,"adducts_params.yaml")

## data

data = path_r_python("../data")

### source

data_source = path_r_python(data, "source")

#### adducts

data_source_adducts = path_r_python(data_source, "adducts")

##### adducts file

data_source_adducts_file = path_r_python(data_source_adducts, "adducts.tsv")

#### neutral losses

data_source_neutral_losses = path_r_python(data_source, "neutral_losses")

##### neutral losses file

data_source_neutral_losses_file = path_r_python(data_source_neutral_losses, "neutral_losses.tsv")

#### databases

data_source_databases = path_r_python(data_source, "databases")

##### lotus

data_source_databases_lotus = path_r_python(data_source_databases, "lotus.csv.gz")

#### demo

data_source_demo = path_r_python(data_source, "demo")

##### benchmark

data_source_demo_benchmark = path_r_python(data_source_demo, "features_benchmark_raw.tsv.gz")

### interim

data_interim = path_r_python(data, "interim")

#### lotus
data_interim_lotus_prepared = path_r_python(data_interim, "lotus_prepared.tsv.gz")

#### dnp
data_interim_dnp_prepared = path_r_python(data_interim, "dnp_prepared.tsv.gz")

### processed

data_processed = path_r_python(data, "processed")

#### adducts pos

data_processed_adducts_pos = path_r_python(data_processed, "adducts_pos.tsv.gz")

#### adducts neg

data_processed_adducts_neg = path_r_python(data_processed, "adducts_neg.tsv.gz")

### docopt

#### prepare_lib

docopt_prepare_lib = "docopt/prepare_lib.txt"

docopt_prepare_dnp = "docopt/prepare_dnp.txt"

docopt_form_adducts = "docopt/prepare_lib.txt"
