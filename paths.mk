# Paths

## config
export DEFAULT_PATH = ${CONFIG_PATH}/default
export PARAMS_PATH = ${CONFIG_PATH}/params

## data
export INTERIM_PATH = ${DATA_PATH}/interim
export PROCESSED_PATH = ${DATA_PATH}/processed
export SOURCE_PATH = ${DATA_PATH}/source

## doc
export REFERENCES_PATH = ${DOC_PATH}/references

## src
export DOCOPT_PATH = ${SRC_PATH}/docopt
export PYTHON_PATH = ${SRC_PATH}/python
export R_PATH = ${SRC_PATH}/R
export SHELL_PATH = ${SRC_PATH}/shell

### SOURCE
export SOURCE_ADDUCTS_PATH = ${SOURCE_PATH}/adducts
export SOURCE_DATABASES_PATH = ${SOURCE_PATH}/databases
export SOURCE_NEUTRAL_LOSSES_PATH = ${SOURCE_PATH}/neutral_losses
export SOURCE_PRIVATE_PATH = ${SOURCE_PATH}/private

### R
export FUNCTIONS_PATH = ${R_PATH}/functions
