include config.mk

.PHONY: get-lotus prepare-lotus prepare-dnp prepare-custom-library form-adducts
.PRECIOUS: %.tsv %.zip %.json %.gz

get-lotus: 
	bash src/shell/get_lotus.sh

prepare-lotus:
	cd src && Rscript R/1_prepare_lotus.R

prepare-dnp:
	cd src && Rscript R/1_prepare_dnp.R

## feel free to prepare all custom libraries you want!

prepare-library:
	cd src && Rscript R/2_prepare_library.R

form-adducts:
	cd src && Rscript R/3_form_adducts.R